import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent {
  @Output() tagsChange = new EventEmitter<string[]>();
  @Input() tags: string[] = [];
  @Input() showX: boolean = true;

  deleteTag(tag: string): void {
    this.tags = this.tags.filter(t => t !== tag);
  }
}
