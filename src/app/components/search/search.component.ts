import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  @Output() searchTags = new EventEmitter<string[]>();
  @Input() tags: string[] = [];
  @Input() showSearchButton: boolean = true;

  searchText: string;

  constructor(private router: Router) {
  }

  public enterClicked(): void {
    if (!this.searchText) {
      this.search();
      return;
    }
    const isTagExist = this.tags.includes(this.searchText);
    if (!isTagExist) {
      this.tags.push(this.searchText);
    }
    this.searchText = '';
  }

  search(): void {
    if (this.tags) {
      this.searchTags.emit(this.tags);
    }
  }
}
