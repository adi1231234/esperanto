import {Component, Input, OnInit} from '@angular/core';
import {DataType} from './interfaces';
import {PreviewService} from '../../services/preview.service';

@Component({
  selector: 'app-data-type',
  templateUrl: './data-type.component.html',
  styleUrls: ['./data-type.component.scss']
})
export class DataTypeComponent implements OnInit {
  @Input() dataType: DataType;

  constructor(public previewService: PreviewService) {
  }

  ngOnInit(): void {
  }

}
