export interface DataType {
  _id?: number;
  title: string;
  type: DataTypeType;
  data: File | string;
  tags?: string[];
}

export type DataTypeType = 'web' | 'image';
