import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SearchPageComponent} from './pages/search-page/search-page.component';
import {ResultsPageComponent} from './pages/results-page/results-page.component';
import {UploadPageComponent} from './pages/upload-page/upload-page.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'search-page'
  },
  {
    path: 'search-page',
    component: SearchPageComponent
  },
  {
    path: 'results-page',
    component: ResultsPageComponent
  },
  {
    path: 'upload-page',
    component: UploadPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
