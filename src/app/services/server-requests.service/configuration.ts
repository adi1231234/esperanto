export const api = {
  search: '/search',
  autoComplete: '/autoComplete',
  upload: '/upload'
};
