import {Injectable} from '@angular/core';
import {DataType} from '../../components/data-type/interfaces';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {api} from './configuration';

@Injectable({
  providedIn: 'root'
})
export class ServerRequestsService {

  constructor(private httpClient: HttpClient) {
  }

  async getDataTypes(tags: string[]): Promise<DataType[]> {
    const result: any = await this.httpClient.post(environment.serverUrl + api.search, {tags}).toPromise();
    return result;
    // return [
    //   {
    //     _id: 123,
    //     data: 'http://chiuchim.co.il/home',
    //     title: 'google',
    //     type: 'web',
    //     tags: ['sdff', 'asdfasd', 'sdfsdf']
    //   },
    //   {
    //     _id: 11111,
    //     data: 'https://q-cf.bstatic.com/images/hotel/max1024x768/616/61661447.jpg',
    //     title: 'תמונה יפה',
    //     type: 'image',
    //     tags: ['2221', '221222', '3334']
    //   }
    // ];
  }

  async upload(dataType: DataType, tags: string[]): Promise<void> {
    await this.httpClient.post(environment.serverUrl + api.upload, {dataType, tags}).toPromise();
    console.log(environment.serverUrl + api.upload, {dataType, tags});
  }
}
