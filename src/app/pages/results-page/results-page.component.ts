import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {DataType} from '../../components/data-type/interfaces';
import {ServerRequestsService} from '../../services/server-requests.service';
import {PreviewService} from '../../services/preview.service';

@Component({
  selector: 'app-results-page',
  templateUrl: './results-page.component.html',
  styleUrls: ['./results-page.component.scss']
})
export class ResultsPageComponent implements OnInit, OnDestroy {

  public queryParamsSubscription: Subscription;

  private _tags: string[] = [];
  get tags(): string[] {
    return this._tags;
  };

  set tags(value: string[]) {
    this._tags = value;
    this.as(value);
  }

  private async as(value) {
    this.dataTypes = await this.serverRequests.getDataTypes(value);
  }

  public stopRefresh = false;
  public dataTypes: DataType[] = null;
  preview: boolean = false;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private serverRequests: ServerRequestsService,
              private previewService: PreviewService) {
  }

  ngOnInit(): void {
    this.preview = this.previewService.preview;
    this.queryParamsSubscription = this.route.queryParams.subscribe((params: Params) => {
      this.tags = params.tags;
      if (!this.tags?.length && !this.stopRefresh) {
        this.router.navigate(['search-page']);
      }
    });
  }

  ngOnDestroy(): void {
    this.queryParamsSubscription.unsubscribe();
  }

  async searchTags(tags: string[]): Promise<void> {
    this.stopRefresh = true;
    await this.router.navigate(
      [],
      {
        queryParams: {tags: null},
        queryParamsHandling: 'merge',
      });
    this.stopRefresh = false;
    await this.router.navigate(
      [],
      {
        queryParams: {tags},
        queryParamsHandling: 'merge',
      });

  }

  changePreview(preview: boolean): void {
    this.preview = preview;
    this.previewService.preview = preview;
  }
}
