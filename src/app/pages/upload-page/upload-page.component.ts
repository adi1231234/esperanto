import {Component, OnInit} from '@angular/core';
import {ServerRequestsService} from '../../services/server-requests.service';
import {DataTypeType} from '../../components/data-type/interfaces';

@Component({
  selector: 'app-upload-page',
  templateUrl: './upload-page.component.html',
  styleUrls: ['./upload-page.component.scss']
})
export class UploadPageComponent implements OnInit {

  public tags: string[] = [];
  public type: DataTypeType = 'web';
  public title: string = '';
  public value: File | string;

  constructor(private serverRequests: ServerRequestsService) {
  }

  ngOnInit(): void {
  }

  async selectedTags(): Promise<void> {
    await this.serverRequests.upload({title: this.title, type: this.type, data: this.value}, this.tags);
  }

  typeChanged(type: DataTypeType): void {
    this.type = type;
  }

  handleFileInput(files: any): void {
    this.value = (files.target.files as FileList).item(0);
  }
}
