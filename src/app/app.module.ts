import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {UploadPageComponent} from './pages/upload-page/upload-page.component';
import {SearchPageComponent} from './pages/search-page/search-page.component';
import {ResultsPageComponent} from './pages/results-page/results-page.component';
import {AppRoutingModule} from './app-routing.module';
import {BarComponent} from './components/bar/bar.component';
import {SearchComponent} from './components/search/search.component';
import {FormsModule} from '@angular/forms';
import {DataTypeComponent} from './components/data-type/data-type.component';
import {HttpClientModule} from '@angular/common/http';
import {LoaderComponent} from './components/loader/loader.component';
import {TagsComponent} from './components/tags/tags.component';

@NgModule({
  declarations: [
    AppComponent,
    UploadPageComponent,
    SearchPageComponent,
    ResultsPageComponent,
    BarComponent,
    SearchComponent,
    DataTypeComponent,
    LoaderComponent,
    TagsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
